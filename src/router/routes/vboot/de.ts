import type { AppRouteModule } from '/@/router/types';

import { LAYOUT } from '/@/router/constant';

const dashboard: AppRouteModule = {
  path: '/de',
  name: 'de',
  component: LAYOUT,
  redirect: '/de/supp/main',
  meta: {
    // hideChildrenInMenu: true,
    icon: 'ant-design:trophy-outlined',
    title: 'DEMO',
    orderNo: 5,
  },
  children: [
    {
      path: 'supp/cate',
      name: 'DeSuppCate',
      component: () => import('/@/pages/de/supp/cate/index.vue'),
      meta: {
        title: '供应商分类',
        ignoreKeepAlive: false,
      },
    },
    {
      path: 'supp/main',
      name: 'DeSuppMain',
      component: () => import('/@/pages/de/supp/main/index.vue'),
      meta: {
        title: '供应商清单',
        ignoreKeepAlive: false,
      },
    },
    {
      path: 'supp/main/edit',
      name: 'DeSuppMainEdit',
      component: () => import('/@/pages/de/supp/main/edit.vue'),
      meta: {
        title: '代理商编辑',
        ignoreKeepAlive: false,
        hideMenu: true,
      },
    },
  ],
};

export default dashboard;
