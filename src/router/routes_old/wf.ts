import type { AppRouteModule } from '/@/router/types';

import { LAYOUT } from '/@/router/constant';

const bi: AppRouteModule = {
  path: '/wf',
  name: 'Wf',
  component: LAYOUT,
  redirect: '/wf/tem/main',
  meta: {
    // hideChildrenInMenu: true,
    icon: 'ant-design:node-index-outlined',
    title: '流程管理',
    orderNo: 3,
  },
  children: [
    // {
    //   path: 'demo1',
    //   name: 'WfDemo1',
    //   component: () => import('/@/pages/wf/demo1/index.vue'),
    //   meta: {
    //     title: '工作流DEMO1',
    //     ignoreKeepAlive: false,
    //     // icon: 'simple-icons:about-dot-me',
    //     // hideMenu: true,
    //   },
    // },
    // {
    //   path: 'viewer',
    //   name: 'WfViewer',
    //   component: () => import('/@/pages/wf/demo1/viewer.vue'),
    //   meta: {
    //     title: 'viewer',
    //     ignoreKeepAlive: false,
    //     // icon: 'simple-icons:about-dot-me',
    //     // hideMenu: true,
    //   },
    // },
    // {
    //   path: 'demo2',
    //   name: 'WfDemo2',
    //   component: () => import('/@/pages/wf/demo2/index.vue'),
    //   meta: {
    //     title: 'DEMO',
    //     ignoreKeepAlive: false,
    //     // icon: 'simple-icons:about-dot-me',
    //     // hideMenu: true,
    //   },
    // },
    {
      path: 'tem/cate',
      name: 'WfTemCate',
      component: () => import('/@/pages/wf/tem/cate/index.vue'),
      meta: {
        title: '流程分类',
        ignoreKeepAlive: false,
        // icon: 'simple-icons:about-dot-me',
        // hideMenu: true,
      },
    },
    {
      path: 'tem/main',
      name: 'WfTemMain',
      component: () => import('/@/pages/wf/tem/main/index.vue'),
      meta: {
        title: '流程模板',
        ignoreKeepAlive: false,
        // icon: 'simple-icons:about-dot-me',
        // hideMenu: true,
      },
    },
    {
      path: 'tem/main/:id',
      name: 'WfTemMainEdit',
      meta: {
        title: '流程模板维护',
        hideMenu: true,
        ignoreKeepAlive: false,
      },
      component: () => import('/@/pages/wf/tem/main/edit.vue'),
    },
    {
      path: 'ins/main',
      name: 'WfInsMain',
      component: () => import('/@/pages/wf/ins/main/index.vue'),
      meta: {
        title: '流程实例',
        ignoreKeepAlive: false,
      },
    },
    {
      path: 'ins/main/:id',
      name: 'WfInsMainEdit',
      meta: {
        title: '流程实例维护',
        hideMenu: true,
        ignoreKeepAlive: false,
      },
      component: () => import('/@/pages/wf/ins/main/edit.vue'),
    },
  ],
};

export default bi;
