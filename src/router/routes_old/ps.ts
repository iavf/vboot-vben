import type { AppRouteModule } from '/@/router/types';

import { LAYOUT } from '/@/router/constant';

const bi: AppRouteModule = {
  path: '/ps',
  name: 'Ps',
  component: LAYOUT,
  redirect: '/ps/proj',
  meta: {
    icon: 'ant-design:compass-outlined',
    title: '项目管理',
    orderNo: 4,
  },
  children: [
    {
      path: 'proj/cate',
      name: 'PsProjCate',
      component: () => import('/@/pages/ps/proj/cate/index.vue'),
      meta: {
        title: '项目分类',
        ignoreKeepAlive: false,
      },
    },
    {
      path: 'proj/main',
      name: 'PsProjMain',
      component: () => import('/@/pages/ps/proj/main/index.vue'),
      meta: {
        title: '项目清单',
        ignoreKeepAlive: false,
      },
    },
    {
      path: 'task/main/:id',
      name: 'PsTaskMain',
      component: () => import('/@/pages/ps/task/main/index.vue'),
      meta: {
        title: '项目跟踪',
        // hideMenu: true,
        ignoreKeepAlive: false,
      },
    },
  ],
};

export default bi;
