import type { AppRouteModule } from '/@/router/types';

import { LAYOUT } from '/@/router/constant';

const bi: AppRouteModule = {
    path: '/bi',
    name: 'Bi',
    component: LAYOUT,
    redirect: '/bi/etl/main',
    meta: {
        // hideChildrenInMenu: true,
        icon: 'ant-design:ant-design-outlined',
        title: '数据可视化 [JAVA]',
        orderNo: 3,
    },
    children: [
        {
            path: 'etl/main',
            name: 'BiEtlMain',
            component: () => import('/@/pages/bi/etl/main/index.vue'),
            meta: {
                title: 'ETL任务',
                ignoreKeepAlive: false,
                // icon: 'simple-icons:about-dot-me',
                // hideMenu: true,
            },
        },
        {
            path: 'etl/log',
            name: 'BiEtlLog',
            component: () => import('/@/pages/bi/etl/log/index.vue'),
            meta: {
                title: 'ETL日志',
                ignoreKeepAlive: false,
                // icon: 'simple-icons:about-dot-me',
                // hideMenu: true,
            },
        },
	  // {
      //       path: 'table/main',
      //       name: 'BiTableMain',
      //       component: () => import('/@/pages/bi/table/main/index.vue'),
      //       meta: {
      //           title: '表维护',
      //           ignoreKeepAlive: false,
      //           // icon: 'simple-icons:about-dot-me',
      //           // hideMenu: true,
      //       },
      //   },
      //   {
      //       path: 'field/main/:id',
      //       name: 'BiFieldMain',
      //       component: () => import('/@/pages/bi/field/main/index.vue'),
      //       meta: {
      //           hideMenu: true,
      //           title: '字段配置',
      //           ignoreKeepAlive: false,
      //       },
      //   },
      //   {
      //       path: 'demo/boy',
      //       name: 'BiDemoBoy',
      //       component: () => import('/@/pages/bi/demo/boy/index.vue'),
      //       meta: {
      //           title: '男生信息',
      //           ignoreKeepAlive: false,
      //           // icon: 'simple-icons:about-dot-me',
      //           // hideMenu: true,
      //       },
      //   },
      //   {
      //       path: 'demo/girl',
      //       name: 'BiDemoGril',
      //       component: () => import('/@/pages/bi/demo/girl/index.vue'),
      //       meta: {
      //           title: '女生信息',
      //           ignoreKeepAlive: false,
      //           // icon: 'simple-icons:about-dot-me',
      //           // hideMenu: true,
      //       },
      //   },
      //   {
      //       path: 'demo/student',
      //       name: 'BiDemoStudent',
      //       component: () => import('/@/pages/bi/demo/student/index.vue'),
      //       meta: {
      //           title: '学生信息',
      //           ignoreKeepAlive: false,
      //           // icon: 'simple-icons:about-dot-me',
      //           // hideMenu: true,
      //       },
      //   },
    ],
};

export default bi;
